=begin
  Exercício para fixação, proposto por mim.
  1. Crie um programa que exiba um menu com as seguintes opções: Listar Usuários, Cadastrar novo usuário, Remover usuário, Editar usuário, Sair.
  2. No cadastrar novo usuário, um novo usuário deverá ser criado e armazenado num vetor na memória. Properties: id, nome, email, senha e tipo (utilize símbolos: :admin, :cliente, :newsletter)
  3. No Listar usuários, deve-se exibir todos os usuários bem como seus dados em lista
  4. No Editar usuários, o sistema deve pedir o id do usuário a ser editado, e as novas informações. Se deixar em branco, mantém-se o original.
  5. No Remover usuário, o sistema deve pedir o id do usuário a ser removido.
  6. Sempre apos cada ação, exibir o menu novamente. Finalizar o programa apenas se o usuário escolher a opção Sair.
=end
load("Core/app.rb")
load("Core/menu_item.rb")
load("Processors/list_usuarios_processor.rb")
load("Processors/cad_usuario_processor.rb")
load("Processors/close_processor.rb")

app = App.new

opt_list_users = MenuItem.new
opt_list_users.menu_id = 1
opt_list_users.menu_label = "Listar Usuários"
opt_list_users.menu_processor = ListUsuariosProcessor.new


opt_cad_user = MenuItem.new
opt_cad_user.menu_id = 2
opt_cad_user.menu_label = "Cadastrar Usuário"
opt_cad_user.menu_processor = CadUsuarioProcessor.new


opt_exit = MenuItem.new
opt_exit.menu_id = 3
opt_exit.menu_label = "Encerrar Programa"
opt_exit.menu_processor = CloseProcessor.new

app.add_menu_item(opt_list_users)
app.add_menu_item(opt_cad_user)
app.add_menu_item(opt_exit)

app.entity_repository = UsuarioRepository.new

app.app_name = "Aplicação de Estudos"
app.execute