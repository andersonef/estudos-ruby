load("Contracts/entity_repository_contract.rb")
load("Entities/usuario.rb")
class UsuarioRepository < EntityRepositoryContract

  def initialize
    @entity_class = Usuario
  end
end