load("Contracts/processor_contract.rb")
class ListUsuariosProcessor < ProcessorContract

  def execute(appInstance)
    puts " \n\n ========== LISTANDO USUÁRIOS =========\n\n"
    if appInstance.entity_repository.all().nil?
      puts "Nenhum usuário cadastrado até o momento!"
      return
    end
    for usuario in appInstance.entity_repository.all()
      puts "##{usuario.id.to_s.ljust(3, " ")} | #{usuario.label.to_s.ljust(20, " ")} | #{usuario.email.to_s.ljust(20, " ")}"
    end
  end
end