load("Contracts/processor_contract.rb")
class CloseProcessor < ProcessorContract

  def execute(appInstance)

    puts "\n\n\n ============ ATENÇÃO ============"
    print "Tem certeza que deseja encerrar? (s/n): "
    response = gets.chomp.to_s

    return if(response == "n")

    if(response == "s")
      puts ".................. Encerrando programa."
      exit
    end

    puts "\n\n OPÇÃO INVÁLIDA!"
    self.execute(appInstance)
  end
end