load("Contracts/processor_contract.rb")
class CadUsuarioProcessor < ProcessorContract

  def execute(appInstance)
    usuario = {}
    puts " \n\n ========== CADASTRO DE USUÁRIOS =========\n\n"

    print "Nome: "
    usuario['nome'] = gets.chomp.to_s

    print "Email: "
    usuario['email'] = gets.chomp.to_s

    loop do
      begin
        puts "Informe o tipo de Usuário: "
        puts " 1 - Administrador"
        puts " 2 - Cliente"
        puts " 3 - Newsletter"

        print "Opção desejada: "
        selected_opt = gets.chomp.to_i
        case
          when selected_opt == 1
            usuario['tipo'] = Usuario::TIPO_ADMIN

          when selected_opt == 2
            usuario['tipo'] = Usuario::TIPO_CLIENTE

          when selected_opt == 3
            usuario['tipo'] = Usuario::TIPO_NEWSLETTER
          else
            raise SignalException
        end
        break
      rescue
        puts "Opção Inválida!"
      end
    end

    appInstance.entity_repository.create(usuario)
    puts "Usuário criado com sucesso!"
  end
end