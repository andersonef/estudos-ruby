load("Repositories/usuario_repository.rb")

class App
  attr_accessor :app_name, :menu_items, :entity_repository


  def initialize
    @menu_items = []
    @app_name = "AppName"
  end


  def add_menu_item(menu_item)
    @menu_items.push(menu_item)
  end

  def execute
    loop do

      puts "\n\n\n========= #{@app_name} ========="
      puts "========= MENU PRINCIPAL ========="
      for menu_item in @menu_items
        puts menu_item.menu_id.to_s + " - " + menu_item.menu_label
      end

      print "Informe a opção desejada: "

      begin
        selected_id = gets.chomp.to_i
        for menu_item in @menu_items
          if(menu_item.menu_id == selected_id)
            menu_item.menu_processor.execute(self)
          end
        end

      rescue SystemExit
        exit(0)
      rescue Exception => e
        puts "Ocorreu um erro." + e.message.to_s
      end
    end
    exit
  end
end