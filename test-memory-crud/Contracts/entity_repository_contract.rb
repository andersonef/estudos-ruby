class EntityRepositoryContract
  attr_accessor :entity_class, :entity_list

  def initialize
    puts "\n\n\n\n inicializei um repository\n\n\n\n\n"
  end

  def all()
    self.entity_list
  end


  def create(hash_data)
    puts "chegou aki"
    instance = @entity_class.new
    instance.id = (self.entity_list.nil?) ? 0 : self.entity_list.length

    puts "instanciou com sucesso"
    hash_data.each_key {|property|
      instance.send("#{property}=", hash_data[property])
    }
    self.entity_list = [] if self.entity_list.nil?
    self.entity_list.push(instance)
    instance
  end
end