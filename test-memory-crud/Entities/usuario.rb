load("Contracts/entity_contract.rb")
class Usuario < EntityContract
  TIPO_ADMIN = :tipoAdmin
  TIPO_CLIENTE = :tipoCliente
  TIPO_NEWSLETTER = :tipoNewsletter

  attr_accessor :nome, :email, :tipo

  def nome=(new_name)
    @label = new_name
    @nome = new_name
  end

end